import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';


const endpoint = 'http://blynk.thecloudcomputing.io:8080/ef4ed2b265744860b2e48f4eab0007f7/update/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'    
  })
};

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }
  
  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  Convertir_slider(led): Observable<any> {
  //  return this.http.put<any>(endpoint + 'V1', 
  //          JSON.stringify(led),
  //          httpOptions).pipe(
  //    //tap((result) => console.log(`la conversion es = ${result.data}`)),
  //    catchError(this.handleError<any>('conversor'))
  //
  //  );
    
    var url = endpoint+"V1"+"?value="+led;
    return this.http
    .get(url);



  }

  Convertir_boton(button): Observable<any> {
    console.log(button);
   // return this.http.put<any>(endpoint + 'V2', 
   //         JSON.stringify(button),
   //         httpOptions).pipe(
   //   //tap((result) => console.log(`la conversion es = ${result.data}`)),
   //   catchError(this.handleError<any>('conversor'))
   // );
    
    var url = endpoint+"V0"+"?value="+button;
    return this.http
    .get(url);



  }
  


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}




