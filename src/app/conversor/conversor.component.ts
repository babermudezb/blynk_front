import { Component, OnInit, Input } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-conversor',
  templateUrl: './conversor.component.html',
  styleUrls: ['./conversor.component.css']
})
export class ConversorComponent implements OnInit {

   datoSend_slider = [""];
   datoSend_button : any;
   marked = false;
   theCheckbox = false;
   //datoReceived ={statusCode:'' ,uni_dat: "", uni_conv:"", dat:"", dat_conv:""};
  datoReceived: any;
  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

  }
  Slider(led) {
    //Convertir() {
    this.datoSend_slider = led; 
    console.log(this.datoSend_slider);
    this.rest.Convertir_slider(this.datoSend_slider).subscribe((result) => {
     // this.productData = result;
      this.datoReceived = result;
      //console.log(result);
      //this.router.navigate(['/product-details/'+result._id]);
    }, (err) => {
      console.log(err);
    });
    
  }
  Boton(button) {
    //Convertir() {
    if (button == false)
    {
      this.datoSend_button = "1";
    }else{
      this.datoSend_button = "0";
    }
    console.log(this.datoSend_button); 
    this.rest.Convertir_boton(this.datoSend_button).subscribe((result) => {
     // this.productData = result;
      this.datoReceived = result;
      //console.log(result);
      //this.router.navigate(['/product-details/'+result._id]);
    }, (err) => {
      console.log(err);
    });
    
  }
  toggleVisibility(e){
    this.marked= e.target.checked;
  }

}
